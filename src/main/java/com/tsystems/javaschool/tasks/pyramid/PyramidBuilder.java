package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    private int pyramidHigh;
    private int pyramidWidth;

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            checkCreationPossibility(inputNumbers.size());
            List<Integer> input = sort(inputNumbers);
            int[][] pyramid = new int[pyramidHigh][pyramidWidth];
            int currentLevel = pyramidHigh;
            int from = input.size() - currentLevel;
            int to = input.size();
            for (int i = pyramidHigh - 1; i >= 0; i--) {
                int[] level = pyramid[i];
                List<Integer> sublist = input.subList(from, to);
                int k = 0;
                for (int j = pyramidHigh - i - 1; j < pyramidWidth - pyramidHigh + i + 1; j += 2) {
                    level[j] = sublist.get(k);
                    k++;
                }
                to = from;
                from = to - i;
            }
            return pyramid;
        } catch (Throwable thr) {
            throw new CannotBuildPyramidException("Error into builder");
        }
    }

    private void checkCreationPossibility(int quantityOfElements) {
        int quantityOfLevels = 1;
        int elementsNeeded = 0;
        while (elementsNeeded < quantityOfElements) {
            elementsNeeded = quantityOfLevels + elementsNeeded;
            quantityOfLevels++;
            if (elementsNeeded == quantityOfElements) {
                pyramidHigh = quantityOfLevels - 1;
                pyramidWidth = pyramidHigh * 2 - 1;
                return;
            }
        }
        throw new CannotBuildPyramidException("Wrong quantity of elements");
    }

    private List<Integer> sort(List<Integer> input) {
        try {
            Collections.sort(input);
            return input;
        } catch (Exception ex) {
            throw new CannotBuildPyramidException("Sorting exception. Probably wrong arguments");
        }
    }
}
