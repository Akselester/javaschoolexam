package com.tsystems.javaschool.tasks.calculator;

class Token {

    private String type;
    private String value;
    private int priority;

    Token(String type, String value, int priority) {
        this.type = type;
        this.value = value;
        this.priority = priority;
    }

    String getType() {
        return type;
    }

    String getValue() {
        return value;
    }

    int getPriority() {
        return priority;
    }
}
