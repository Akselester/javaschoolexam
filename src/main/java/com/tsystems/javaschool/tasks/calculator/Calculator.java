package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.stream.IntStream;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     * parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private static final List<Character> LEGAL_CHARACTERS = new ArrayList<>(Arrays.asList('(', ')', '+', '-', '*', '/', '.', '0', '1', '2', '3',
            '4', '5', '6', '7', '8', '9'));
    private List<Token> parsedStatement = new ArrayList<>();
    private Stack<Token> operationStack = new Stack<>();

    public String evaluate(String statement) {
        try {
            if (!validateStatement(statement)) {
                return null;
            }
            parseStatement(statement);
            return calculate();
        } catch (Exception ex) {
            return null;
        } finally {
            parsedStatement.clear();
            operationStack.clear();
        }
    }

    private String calculate() {
        operationStack.clear();
        IntStream.range(0, parsedStatement.size()).forEach(i -> {
            if (!parsedStatement.get(i).getType().equals("operator")) {
                operationStack.push(parsedStatement.get(i));
            } else {
                operationStack.push(new Token("number", doOperation(parsedStatement.get(i).getValue()), 0));
            }
        });
        double result = Double.parseDouble(operationStack.pop().getValue());
        result = new BigDecimal(result).setScale(4, RoundingMode.HALF_EVEN).doubleValue();
        String answer = Double.toString(result);
        if (answer.endsWith(".0")) {
            answer = answer.split("\\.")[0];
        }
        return answer;
    }

    private String doOperation(String operation) {
        double rightVar = Double.parseDouble(operationStack.pop().getValue());
        double leftVar = Double.parseDouble(operationStack.pop().getValue());
        double result = 0;
        switch (operation) {
            case ("plus"):
                result = leftVar + rightVar;
                break;
            case ("minus"):
                result = leftVar - rightVar;
                break;
            case ("mult"):
                result = leftVar * rightVar;
                break;
            case ("div"):
                result = leftVar / rightVar;
                break;
        }
        return Double.toString(result);
    }

    private boolean validateStatement(String statement) {
        if (statement == null) {
            return false;
        } else if (!parenthesesCheck(statement)) {
            return false;
        } else return !statement.equals("");
    }

    private boolean parenthesesCheck(String statement) {
        long openedParentheses = statement.codePoints().filter(value -> value == '(').count();
        long closedParentheses = statement.codePoints().filter(value -> value == ')').count();
        return (openedParentheses - closedParentheses) == 0;
    }

    private void parseStatement(String statement) {
        String value = "";
        boolean dotInNumber = false;
        for (int statementPosition = 0; statementPosition < statement.length(); statementPosition++) {
            char symbol = statement.charAt(statementPosition);

            if (LEGAL_CHARACTERS.contains(symbol)) {
                if (Character.isDigit(symbol)) {
                    value += symbol;
                } else if (symbol == '.') {
                    if ((value.equals("")) || (dotInNumber)) {
                        throw new IllegalArgumentException("Wrong characters sequence.");
                    }
                    value += symbol;
                    dotInNumber = true;
                } else {
                    if (!value.equals("")) {
                        parsedStatement.add(new Token("number", value, 0));
                        value = "";
                        dotInNumber = false;
                    }
                    switch (symbol) {
                        case ('('):
                            operationStack.push(new Token("op", "(", 0));
                            break;
                        case (')'):
                            while (!operationStack.peek().getType().equals("op")) {
                                parsedStatement.add(operationStack.pop());
                            }
                            operationStack.pop();
                            break;
                        case ('+'):
                            dispatchToken(new Token("operator", "plus", 1));
                            break;
                        case ('-'):
                            dispatchToken(new Token("operator", "minus", 1));
                            break;
                        case ('*'):
                            dispatchToken(new Token("operator", "mult", 2));
                            break;
                        case ('/'):
                            dispatchToken(new Token("operator", "div", 2));
                            break;
                    }
                }
            } else {
                throw new IllegalArgumentException("Symbol " + symbol + " is illegal.");
            }
        }
        if (!value.equals("")) {
            parsedStatement.add(new Token("number", value, 0));
        }
        if (!operationStack.empty()) {
            for (int i = operationStack.size(); i > 0; i--) {
                parsedStatement.add(operationStack.pop());
            }
        }
    }

    private void dispatchToken(Token token) {
        while ((!operationStack.empty()) && (operationStack.peek().getType().equals("operator"))) {
            if (operationStack.peek().getPriority() >= token.getPriority()) {
                parsedStatement.add(operationStack.pop());
            } else {
                break;
            }
        }
        operationStack.push(token);
    }

}
