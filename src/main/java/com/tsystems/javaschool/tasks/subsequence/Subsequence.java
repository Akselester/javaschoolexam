package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        check(x, y);
        if (x.size() > y.size()) {
            return false;
        }
        int k = 0;
        for (Object obj : x) {
            for (int j = k; j < y.size(); j++) {
                if (y.get(j).equals(obj)) {
                    k = j + 1;
                    break;
                }
                if (j == y.size() - 1) {
                    return false;
                }
            }
        }
        return true;
    }

    @SuppressWarnings("rawtypes")
    private void check(List x, List y) {
        if ((x == null) || (y == null)) {
            throw new IllegalArgumentException("Illegal arguments");
        }
    }
}
